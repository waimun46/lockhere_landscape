/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Platform,
  PermissionsAndroid,
  ActivityIndicator,
  Image,
  Dimensions, Animated, Easing, PixelRatio, 
} from 'react-native';
import { NoticeBar } from '@ant-design/react-native';
import AsyncStorage from '@react-native-community/async-storage';
import LottieView from 'lottie-react-native';
import Video from 'react-native-video';
import DeviceInfo from 'react-native-device-info';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

import logo from './assets/logo/logo.jpg';
import qr from './assets/QRphoto.png';

import cloudys from './assets/lottie/cloudy.json';
import clears from './assets/lottie/clear.json';
import mists from './assets/lottie/mist.json';
import rains from './assets/lottie/rain.json';
import snows from './assets/lottie/snow.json';
import storms from './assets/lottie/storm.json';
import windys from './assets/lottie/windy.json';

const { width, height } = Dimensions.get('window');
const DEVICE_WIDTH = Dimensions.get('window').width;



class App extends Component {
  scrollRef = React.createRef();

  constructor(props) {
    super(props);
    this.state = {
      latitude: 0,
      longitude: 0,
      size: { width, height },
      paused: true,
      selectIndex: 0,
      currentDate: '',
      currentTime: '',
      error: null,
      temperature: 0,
      weatherCondition: null,
      catcherr: null,
      data: [],
      weatherIcon: '',
      isLoadingTem: true,
      progress: new Animated.Value(0),
      loop: true,
      uuid: '',
      mediaList: [],
      storeList: [],
      videoList: [],
      total_run: 0,
      myStatus: 1,
      deviceLoaction: [],
      marquee: [],
      barcode: []
    };
  }


  componentDidMount() {

    this.getUUID();
    this.getCurrentDate();
    this.barcodeFetch();

    /************************************************ Animated weather icon *************************************************/
    this.runAnimation()

    /************************************************ setInterval for scrollView slider *************************************************/
    this.nextSlide();

    /************************************************ AsyncStorage getItem STORE_LIST *************************************************/
    // AsyncStorage.getItem("STORE_LIST").then(mediaListStore => {
    //   //console.log(mediaListStore, '----------------mediaListStore')
    //   const mediaList = JSON.parse(mediaListStore)
    //   if(mediaList.length > 0){
    //     this.setState({
    //       mediaList: mediaList,
    //       isLoading: false,
    //       selectIndex: -1,
    //     }, () => this.nextSlide())
    //   }
    // })

    /************************************************ setInterval get new data *************************************************/
    setInterval(() => {
      this.getMediaList();
      this.barcodeFetch();
    }, 1800000)

    /************************************************ setInterval get new data *************************************************/
    setInterval(() => {
      this.updateStatus();
      this.marqueeText();
    }, 900000)


  }

  componentWillMount() {
    /************************************************ getCurrentTime run *************************************************/
    setInterval(() => this.getCurrentTime(), 1000);
  }

  /************************************************ marqueeText *************************************************/
  marqueeText(uuid = this.state.uuid, ) {
    let url = `https://www.ktic.com.my/lookHere/api/marquee.php?security_code=36BBA69CBEA56FBFE09E17B31C26D57B&uid=${uuid}`;
    // console.log('uri-----marquee', url);
    console.log('marqueeText----');
    fetch(url).then((res) => res.json())
      .then((fetchMarquee) => {
        this.setState({
          marquee: fetchMarquee[0]
        })
      })
  }

  /************************************************ barcodeFetch *************************************************/
  barcodeFetch() {
    let url = "https://www.ktic.com.my/lookHere/api/logo.php?security_code=36BBA69CBEA56FBFE09E17B31C26D57B";
    // console.log('uri-----barcodeFetch', url);
    console.log('barcodeFetch----');
    fetch(url).then((res) => res.json())
      .then((fetchBarcode) => {
        this.setState({
          barcode: fetchBarcode[0]
        })
      })
  }

  /************************************************ updateStatus *************************************************/
  updateStatus(uuid = this.state.uuid, status = this.state.myStatus) {
    let url = `https://www.ktic.com.my/lookHere/api/device_mode.php?security_code=36BBA69CBEA56FBFE09E17B31C26D57B&uid=${uuid}&dev_mode=${status}`;
    // console.log('url-----updateStatus', url);
    console.log('updateStatus-----');
    fetch(url).then((res) => res.json())
      .then((statusUpdate) => {
        console.log('statusUpdate----', statusUpdate[0].status)
      })
  }


  /************************************************ getUUID *************************************************/
  getUUID() {
    let uniqueId = DeviceInfo.getUniqueId();
    console.log('uniqueId------', uniqueId)
    AsyncStorage.setItem('STORE_UUID', uniqueId);
    this.setState({
      uuid: uniqueId,
    }, () => { this.sendUUID(), this.getMediaList(), this.updateStatus(), this.marqueeText() })
  }

  /************************************************ sendUUID *************************************************/
  sendUUID(uuid = this.state.uuid) {
    console.log('sendUUID=======================================', uuid)
    let url = `https://www.ktic.com.my/lookHere/api/device_setup.php?security_code=36BBA69CBEA56FBFE09E17B31C26D57B&uid=${uuid}`;
    fetch(url).then((res) => res.json())
      .then((sendID) => {
        if (sendID[0].status === 1) {
          // alert('Success Get Current Device UniqueId')
          console.log('Success Get Current Device UniqueId', Platform.OS === 'ios' ? `IOS: ${uuid}` : `Android: ${uuid}`)
        } else {
          alert('Failure Get Current Device UniqueId')
        }
      })
  }

  /************************************************ getMediaList *************************************************/
  getMediaList(uuid = this.state.uuid) {
    // console.log('sendUUID====getMediaList', uuid)
    let url = `https://www.ktic.com.my/lookHere/api/media_list_landscape.php?security_code=36BBA69CBEA56FBFE09E17B31C26D57B&uid=${uuid}`;
    fetch(url).then((res) => res.json())
      .then((fetchData) => {
        // console.log('fetchData', fetchData)
        // console.log('this.state.mediaList',this.state.mediaList)
        // console.log('JSON.stringify(fetchData) != JSON.stringify(this.state.mediaList)', JSON.stringify(fetchData) != JSON.stringify(this.state.mediaList))
        if (JSON.stringify(fetchData) != JSON.stringify(this.state.mediaList)) {
          AsyncStorage.setItem('STORE_LIST', JSON.stringify(fetchData));
          this.setState({
            mediaList: fetchData,
            deviceLoaction: fetchData[0],
            isLoading: false,
            selectIndex: -1,
          }, () => { this.nextSlide(), this.fetchWeather() })
        }
      })
  }

  /********************************************* Fetch Weather in Api *********************************************/
  fetchWeather(lat = this.state.deviceLoaction.dev_latitude, lon = this.state.deviceLoaction.dev_longitude, API_KEY = '7a522fdd67758b65b2eb6872f9f08d45') {
    //console.log(lat, lon, '---------------lat long')
    fetch(
      `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&APPID=${API_KEY}&units=metric`
    )
      .then((response) => {
        //console.log(response, 'response----------');
        if (!response.ok) throw new Error(response.status);
        else return response.json();
      })
      .then(json => {
        //console.log(JSON.stringify(json));
        this.setState({
          temperature: json.main.temp.toFixed(0),
          weatherCondition: json.weather[0].main,
          weatherIcon: json.weather[0].icon,
          data: json,
          isLoadingTem: false
        })
      })
      .catch((error) => {
        //console.log(error, 'response----------error');
        this.setState({
          catcherr: error.message
        });
      });
  }

  /************************************************ getCurrentDate *************************************************/
  getCurrentDate() {
    let date = new Date().getDate();
    let month = new Date().getMonth() + 1;
    let year = new Date().getFullYear();
    let d = new Date();
    let weekday = new Array(7);
    weekday[0] = "Sun";
    weekday[1] = "Mon";
    weekday[2] = "Tue";
    weekday[3] = "Wed";
    weekday[4] = "Thu";
    weekday[5] = "Fri";
    weekday[6] = "Sat";
    let day = weekday[d.getDay()];
    this.setState({
      currentDate: day + ' ' + date + '-' + month + '-' + year,
    });
  }

  /************************************************ getCurrentTime *************************************************/
  getCurrentTime() {
    let date, TimeType, hour, minutes, seconds, fullTime;
    date = new Date();
    hour = date.getHours();
    minutes = date.getMinutes();
    seconds = date.getSeconds();
    if (hour <= 11) { TimeType = 'AM'; }
    else { TimeType = 'PM'; }
    if (hour > 12) { hour = hour - 12; }
    if (hour == 0) { hour = 12; }
    if (minutes < 10) { minutes = '0' + minutes.toString(); }
    if (seconds < 10) { seconds = '0' + seconds.toString(); }
    fullTime = hour.toString() + ':' + minutes.toString() + ':' + seconds.toString() + ' ' + TimeType.toString();
    this.setState({
      currentTime: fullTime
    });
  }

  /************************************************ setSelectIndex for scrollView slider *************************************************/
  setSelectIndex = event => {
    const viewSize = event.nativeEvent.layoutMeasurement.width;
    const contentOffset = event.nativeEvent.contentOffset.x;
    const selectIndex = Math.floor(contentOffset / viewSize);
    this.setState({
      selectIndex
    })
  }

  /************************************************ nextSlide *************************************************/
  nextSlide() {
    console.log('nextSlide run');
    // console.log(this.state.selectIndex, 'nextSlide run this.state.selectIndex');
    // console.log(this.state.mediaList, 'this.state.mediaList');
    // console.log(this.state.total_run, "total_run")

    clearTimeout(this.interval)

    // if (this.state.mediaList.length === 0 || this.state.total_run >=10) {
    if (this.state.mediaList.length === 0) {
      ;
    } else {
      console.log('nextSlide run before setState');
      this.setState({
        total_run: this.state.total_run + 1,
        selectIndex: this.state.selectIndex === this.state.mediaList.length - 1 ? 0 : this.state.selectIndex + 1,
      },
        () => {
          // console.log('nextSlide run after setState');
          // console.log(this.state.selectIndex, 'nextSlide run this.state.selectIndex');
          let cutWidth = DEVICE_WIDTH
          this.scrollRef.current.scrollTo({
            animated: true,
            y: 0,
            x: cutWidth * this.state.selectIndex,
          })
          if (this.state.mediaList[this.state.selectIndex].md_type === 2) {
            ;
          } else {
            this.interval = setTimeout(() => {
              // console.log(this.state.mediaList, 'this.state.mediaList');
              // console.log(this.state.selectIndex, 'this.state.selectIndex');
              // console.log(this.state.mediaList[this.state.selectIndex].md_duration, 'md_duration')
              this.nextSlide();
            }, this.state.mediaList[this.state.selectIndex].md_duration)
          }
        });
    }
  }

  /************************************************ videoEnd for video end *************************************************/
  videoEnd = (index) => {
    console.log('videoEnd=============');
    setTimeout(() => {
      this.nextSlide();
    }, 100);
    setTimeout(() => {
      this.state.videoList[index].seek(0);
    }, 500);
  }

  /************************************************ runAnimation for icon weather *************************************************/
  runAnimation() {
    Animated.timing(this.state.progress, {
      toValue: 1,
      duration: 3000,
      easing: Easing.linear,
    }).start(() => {
      this.runAnimation();
    });
  }

  /************************************************ setAnim for icon weather *************************************************/
  setAnim = anim => {
    this.anim = anim;
  };





  render() {
    const { currentDate, temperature, data, weatherIcon, weatherCondition, isLoadingTem, mediaList, isLoading, progress,
      loop, size, marquee, barcode, currentTime } = this.state;
    console.log('mediaList----', mediaList)
    console.log('marquee--------', marquee.marq_content)

    const pixelSize = PixelRatio.getPixelSizeForLayoutSize(33);

    return (
      <View style={{ position: 'relative', flex: 1, }}>
        <StatusBar hidden={true} />

        <View style={{ flexDirection: 'row', flex: 1 }}>
          <View style={{ width: '15%', backgroundColor: '#fff' }}>



            <View style={{ height: '30%' }}>
              <View style={{ backgroundColor: '#000', width: '100%', padding: 5, }}>
                <Text style={{ color: '#fff', fontSize: RFPercentage(2), fontWeight: 'bold' }}>ADVERTISE</Text>
              </View>
              <View style={{ marginTop: 0, paddingLeft: 5 }}>
                <Text style={{ color: '#000', fontSize: RFPercentage(2), fontWeight: 'bold' }}>HERE</Text>
              </View>
              <View style={{ width: '100%', height: '50%', paddingRight: 2, marginTop: 5 }} >
                <Image source={{ uri: barcode.logo }} style={{ width: '100%', height: '100%', resizeMode: 'contain', }} />
              </View>
            </View>



            <View style={{ width: '100%', height: '30%', }}>
              <Image source={logo} style={{ width: '100%', height: '100%', }} />
            </View>

            <View style={{ width: '100%', backgroundColor: '#fff', bottom: 0, position: 'absolute', height: '40%',}} >
              <View style={{ width: '95%', }}>
                <View style={{ width: '100%',  paddingTop: 5, paddingBottom: 5, paddingLeft: 5 }}>
                  <Text style={{ textAlign: 'right', fontSize: RFPercentage(1.8), }}>{currentTime}</Text>
                  <Text style={{ textAlign: 'right', fontSize: RFPercentage(1.8), }}>{currentDate}</Text>
                </View>
                {
                  isLoadingTem ?
                    <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
                      <ActivityIndicator size="large" color="#ccc" />
                    </View>
                    :
                    <View style={{ width: '100%', }}>
                      <View style={{ width: '100%', paddingLeft: 5, }}>
                        <LottieView
                          ref={this.setAnim}
                          source={
                            weatherCondition === 'Clear' ? clears :
                              weatherCondition === 'Rain' ? rains :
                                weatherCondition === 'Thunderstorm' ? storms :
                                  weatherCondition === 'Snow' ? snows :
                                    weatherCondition === 'Mist' ? mists :
                                      weatherCondition === 'Clouds' ? cloudys :
                                        windys
                          }
                          progress={progress}
                          loop={loop}
                          autoPlay={progress}
                          style={{ justifyContent: 'center', alignItems: 'center', width: '80%', }}
                          resizeMode="cover"
                        />
                        {/* <Image source={{ uri: `http://openweathermap.org/img/w/${weatherIcon}.png` }}
                          style={{ width: '100%', height: '100%', textAlign: 'center', }} />
                        <Text>{weatherCondition}</Text> */}
                      </View>
                      <View style={{ width: '100%',  marginTop: -10}}>
                        <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                          <Text adjustsFontSizeToFit={true} style={{ fontSize: RFPercentage(4), textAlign: 'center', }}>
                            {temperature}
                          </Text>
                          <Text style={{ fontSize: RFPercentage(3), }} >°</Text>
                        </View>
                      </View>
                    </View>
                }
              </View>
            </View>



          </View>
          {/* <View style={{ width: this.state.size.width - '20%' }}> */}

          <View style={{ width: '85%'}}>

            {/********************************************* slider **********************************************/}
            {
              isLoading ?
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
                  <ActivityIndicator size="large" color="#de2d30" />
                </View>
                :
                <ScrollView
                  horizontal={true}
                  pagingEnabled
                  automaticallyAdjustContentInsets={true}
                  onMomentumScrollEnd={this.setSelectIndex}
                  ref={this.scrollRef}
                  contentContainerStyle={{ flexGrow: 1, }}
                // style={{ height: '100%', width: this.state.size.width  }}
                >
                  <View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap', }}>
                    {
                      mediaList.map((item, index) => {
                        return (
                          item.md_type === 2 ?
                            <View style={[styles.container, { width: this.state.size.width, height: this.state.size.height - 50 }]}>
                              <Video
                                ref={(ref) => {
                                  this.state.videoList[index] = ref
                                }}
                                onEnd={this.videoEnd.bind(this, index)}
                                resizeMode='contain'
                                source={{ uri: item.md_videoPath }}
                                style={[styles.mediaPlayer, { width: this.state.size.width + 90 }]}
                                volume={1}
                                paused={this.state.selectIndex === index ? false : true}
                                // rate={15.0}
                              />
                            </View>
                            :
                            <View style={{ width: this.state.size.width - 50, height: this.state.size.height - 50, }}>
                              <Image source={{ uri: item.md_imagePath }} style={{ width: '100%', height: '100%', resizeMode: 'stretch', }} />
                            </View>
                        )
                      })
                    }
                  </View>
                </ScrollView>
            }

            {/******************************** NoticeBar *********************************/}
            <View style={{ position: 'absolute', bottom: 0, width: '100%', height: 50, }}>
              <NoticeBar
                // onPress={() => alert('click')}
                marqueeProps={{ loop: true,  fps: 150, style: { fontSize: RFPercentage(2.6), color: 'yellow', } }}
                style={{ height: 50, backgroundColor: '#000', color: 'yellow', }}
                // icon={false}
                icon={null}
              >
                {marquee.marq_content}
               
              </NoticeBar>
            </View>
          </View>
        </View>




      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
  mediaPlayer: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'black',

  },

  bannerwarp: { paddingVertical: 3, position: 'relative' },
  bannertextwarp: { position: 'absolute', top: 10, flexDirection: 'row', padding: 10, },
  bannerSty: { width: '100%', height: '100%', resizeMode: 'contain', },




})

export default App;




